﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoInitializer : IDbInitializer
    {
        private IMongoDatabase _mongodb;

        public MongoInitializer(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            _mongodb = client.GetDatabase(settings.DatabaseName);
        }

        public void InitializeDb()
        {
            _mongodb.DropCollection("Customer");
            _mongodb.DropCollection("CustomerPreference");
            _mongodb.DropCollection("Preference");
            _mongodb.DropCollection("PromoCode");
            _mongodb.DropCollection("PromoCodeCustomer");

            _mongodb.GetCollection<Customer>("Customer").InsertMany(FakeDataFactory.Customers);
            _mongodb.GetCollection<Preference>("Preference").InsertMany(FakeDataFactory.Preferences);

            foreach (var cust in FakeDataFactory.Customers)
            {
                if (cust.Preferences != null)
                {
                    _mongodb.GetCollection<CustomerPreference>("CustomerPreference").InsertMany(cust.Preferences);
                }

                if (cust.PromoCodes != null)
                {
                    _mongodb.GetCollection<PromoCodeCustomer>("PromoCodeCustomer").InsertMany(cust.PromoCodes);
                }
            }
        }
    }
}
