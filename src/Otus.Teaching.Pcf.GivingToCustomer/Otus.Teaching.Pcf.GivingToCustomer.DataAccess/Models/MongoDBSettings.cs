﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Models
{
    public class MongoDBSettings : IMongoDBSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
